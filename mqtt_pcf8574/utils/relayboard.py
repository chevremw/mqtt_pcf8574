# -*- coding: utf-8 -*-

# This file is part of mqtt_pcf8574.
#
# mqtt_pcf8574 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_pcf8574 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_pcf8574.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

from .pcf8574 import PCF8574
import lightpiio as pio
from typeguard import typechecked

class RelayBoard():
    
    def __init__(self, i2cbus : pio.I2C, address : int):
        """
        Constructor
        
        :param i2cbus: An instance of I2C bus
        :type i2cbus: lightpiio.I2C
        :param address: Board address on 7 bit. Additional bits are masked.
        :type address: int
        """
        
        self.__address = address
        self._pcf8574 = PCF8574(i2cbus, address)
        
    @property
    def relayStates(self):
        """
        Return the state of the relays on the board
        
        :return: Dictionnary with current state of each relay
        :rtype: dict[int: bool]
        """
        
        v = self._pcf8574.value
        r = dict()
        
        for i in range(8):
            r[f"r{i+1}"] = (v & (1 << i)) == 0
            
        return r
    
    def setRelayStates(self, **states):
        """
        Set the states of individual relays. Relays not in list are not affected.
        
        :param **states: Dictionnary of relay id (r1..r8) and bool
        :type **states: dict[string, bool]
        """
        
        pcfv = self._pcf8574.value
        
        for k, v in states.items():
            if len(k) != 2 or k[0] != 'r':
                raise ValueError("Valid arguments keys are r1..r8")
                
            rid = int(k[1])
            
            if rid < 0 or rid > 8:
                raise ValueError("Valid arguments keys are r1..r8")
                
            if not isinstance(v, bool):
                raise ValueError("Values must be boolean")
                
            if v:
                pcfv &= 0xff - (1 << (rid-1)) # Write a 0 to enable relay
            else:
                pcfv |= (1 << (rid-1)) # Write a 1 to disable relay
                
        
        self._pcf8574.value = pcfv
        
    def on(self, *args):
        """
        Enable relays specified by id
        
        :param *args: List of relay id from 1 to 8
        :type *args: list[int]
        """
        
        msk = 0
        for r in args:
            if not isinstance(r, int):
                raise ValueError(f"Unknown relay r{r}")
                
            if r < 0 or r > 8:
                raise ValueError(f"Unknown relay r{r}")
                
            msk |= 1 << (r-1)
            
        self._pcf8574.clearMask(msk)
        
    def off(self, *args):
        """
        Disable relays specified by id
        
        :param *args: List of relay id from 1 to 8
        :type *args: list[int]
        """
        
        msk = 0
        for r in args:
            if not isinstance(r, int):
                raise ValueError(f"Unknown relay r{r}")
                
            if r < 0 or r > 8:
                raise ValueError(f"Unknown relay r{r}")
                
            msk |= 1 << (r-1)
            
        self._pcf8574.setMask(msk)
        
    def toggle(self, *args):
        """
        Toggle relays specified by id
        
        :param *args: List of relay id from 1 to 8
        :type *args: list[int]
        """
        
        msk = 0
        for r in args:
            if not isinstance(r, int):
                raise ValueError(f"Unknown relay r{r}")
                
            if r < 0 or r > 8:
                raise ValueError(f"Unknown relay r{r}")
                
            msk |= 1 << (r-1)
            
        self._pcf8574.toggleMask(msk)
        
            
        
        
