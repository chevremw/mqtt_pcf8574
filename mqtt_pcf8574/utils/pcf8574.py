# -*- coding: utf-8 -*-

# This file is part of mqtt_pcf8574.
#
# mqtt_pcf8574 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_pcf8574 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_pcf8574.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

import lightpiio as pio
from typeguard import typechecked
import gevent

import logging
logger = logging.getLogger(__name__)

class PCF8574():

    NRETRY = 5
    RETRY_DELAY = 1
    
    @typechecked
    def __init__(self, i2cbus : pio.I2C, address : int):
        """
        Constructor
        
        :param i2cbus: An instance of I2C bus
        :type i2cbus: lightpiio.I2C
        :param address: Board address on 7 bit. Additional bits are masked.
        :type address: int
        """
        self.__bus = i2cbus
        self.__address = address&0x7f
        
    @property
    def value(self):
        """
        Get actual value of register
        
        :return: PCF8574 register value
        :rtype: int
        """
        for i in range(self.NRETRY):
            try:
                s = self.__bus.requestFrom(self.__address, 1)
                break
            except pio.i2c.I2CException as ex:
                if i == self.NRETRY-1:
                    raise
                logger.warning(f"Comm error #{i} {ex}")
                gevent.sleep(self.RETRY_DELAY)

        return s[0]
    
    @value.setter
    def value(self, val : int):
        """
        Set register value
        
        :param val: New output of PCF8574 on 8 bit. Additional bits are masked.
        :type val: int
        """
        
        val &= 0xff
        for i in range(self.NRETRY):
            try:
                self.__bus.writeTo(self.__address, [val,])
                break
            except pio.i2c.I2CExcpetion as ex:
                if i == self.NRETRY-1:
                    raise
                logger.warning(f"Comm error #{i} {ex}")
                gevent.sleep(self.RETRY_DELAY)
        
    def setMask(self, mask : int = 0x00):
        """
        Set output bits according to mask.
        
        :param mask: Bits to be affected, defaults to 0x00
        :type mask: int, optional
        """
        
        mask &= 0xff
        self.value |= mask
        
    def clearMask(self, mask : int = 0x00):
        """
        Clear output bits according to mask.
        
        :param mask: Bits to be affected, defaults to 0x00
        :type mask: int, optional
        """
        
        mask &= 0xff
        self.value &= 0xff - mask
        
    def toggleMask(self, mask : int = 0x00):
        """
        Toggle output bits according to mask.
        
        :param mask: Bits to be affected, defaults to 0x00
        :type mask: int, optional
        """
        
        mask &= 0xff
        self.value ^= mask
        

