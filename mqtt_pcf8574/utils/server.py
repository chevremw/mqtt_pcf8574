# -*- coding: utf-8 -*-

# This file is part of mqtt_pcf8574.
#
# mqtt_pcf8574 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_pcf8574 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_pcf8574.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


from paho.mqtt import client as mqtt_client
import re
import logging
logger = logging.getLogger(__name__)
from ..utils.relayboard import RelayBoard
import lightpiio as pio
import time, random

class Server():
    
    def __init__(self, cfg):
        
        self._broker_cfg = cfg['broker']
        self._i2cbus_device = cfg['i2cbus']
        self._boards_cfg = cfg['boards']
        self._mqtt_cfg = cfg['mqtt']
        
        self._main_topic = self._mqtt_cfg['main_topic']
        self._boards = {}
        self._bus = pio.I2C(self._i2cbus_device)
        self._bus.open()
        
        for b in self._boards_cfg:
            B = {
                    'name': b['name'],
                    'location': b.get('location', None),
                    'address': b['address'],
                    'board': RelayBoard(self._bus, b['address'])
                }
            self._boards[B['name']] = B
            
            
        self._client = mqtt_client.Client(self._mqtt_cfg.get('client_id', f'relayboard-{random.randint(0, 1000)}'))
        self._client.username_pw_set(self._broker_cfg.get('username', None), self._broker_cfg.get('password', None) )
        self._client.on_connect = self.on_connect
        self._client.on_disconnect = self.on_disconnect
        self._client.on_message = self.on_message
        
    def connect(self):
        self._client.connect(self._broker_cfg.get('host', None), self._broker_cfg.get('port', None) )
        self._client.subscribe(f"{self._main_topic}/#")

        
    def run(self):
        self.connect()
        self._client.loop_forever()

    def shutdown(self):
        # Send the unavailable state for all relays

        logger.info("Shutting down MQTT server")

        for k, b in self._boards.items(): # Announce unavailability first
            rst = b['board'].relayStates
            for i in range(8):
                self._client.publish(f"{self._main_topic}/{k}/r{i+1}/available", 'OFFLINE', retain=True)

        self._client.disconnect()

    def on_connect(self, client, userdata, flags, rc ):
        if rc == 0:
            logger.info("Connected to MQTT broker")
            
            for k, b in self._boards.items():
                rst = b['board'].relayStates
                for i in range(8):
                    self._client.publish(f"{self._main_topic}/{k}/r{i+1}/available", 'ONLINE', retain=True)
                    self._client.publish(f"{self._main_topic}/{k}/r{i+1}/state", 'ON' if rst[f"r{i+1}"] else 'OFF', retain=True)
            
        else:
            logger.error(f"Failed to connect to MQTT broker ({rc})")
            
    
    def on_disconnect(client, userdata, rc):

        if rc == 0: # The disconnection come from disconnect() method, do not try to reconnect
            return

        FIRST_RECONNECT_DELAY = 1
        RECONNECT_RATE = 2
        MAX_RECONNECT_COUNT = 12
        MAX_RECONNECT_DELAY = 60
    
        logger.info("Disconnected with result code: %s", rc)
        reconnect_count, reconnect_delay = 0, FIRST_RECONNECT_DELAY
        while reconnect_count < MAX_RECONNECT_COUNT:
            logger.info("Reconnecting in %d seconds...", reconnect_delay)
            time.sleep(reconnect_delay)
    
            try:
                client.reconnect()
                logging.info("Reconnected successfully!")
                return
            except Exception as err:
                logging.error("%s. Reconnect failed. Retrying...", err)
    
            reconnect_delay *= RECONNECT_RATE
            reconnect_delay = min(reconnect_delay, MAX_RECONNECT_DELAY)
            reconnect_count += 1
        logger.info("Reconnect failed after %s attempts. Exiting...", reconnect_count)

            
    def on_message(self, client, userdata, msg):
        
        topic = msg.topic
        
        if topic.startswith(self._main_topic):
            subtopic = topic[len(self._main_topic)+1:].split('/')
    
            if len(subtopic) == 2:
                board = subtopic[0]
                relay = subtopic[1]
                
                if board in self._boards:
                    reg = re.compile(r'r([1-8])')
                    M = reg.match(relay)
                    if M is not None:
                        rid = int(M.group(1))
                        pl = msg.payload.decode()
                        
                        B = self._boards[board]['board']
                        
                        if pl == 'ON':
                            B.on(rid)
                        elif pl == 'OFF':
                            B.off(rid)
                        else:
                            logger.error(f"Unknown state {pl}")
                            
                        rst = B.relayStates
                        self._client.publish(f"{self._main_topic}/{board}/r{rid}/state", 'ON' if rst[f"r{rid}"] else 'OFF', retain=True)
                            
                    else:
                        logger.error(f"Unknown relay {relay}")
                        
                else:
                    logger.error(f"Unknown board {board}")
                
        #     else:
        #         logger.warning(f"Unknown topic structure {topic}")
            
        # else:
        #     logger.warning(f"Unknown topic {topic}")
        
        
            
