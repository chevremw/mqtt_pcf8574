# -*- coding: utf-8 -*-

# This file is part of mqtt_pcf8574.
#
# mqtt_pcf8574 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_pcf8574 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_pcf8574.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont



import yaml
import argparse
from ..utils.server import Server
import signal

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="YAML config file")
    args = parser.parse_args()
    
    cfg = None
    with open(args.config,'r') as fd:
        cfg = yaml.safe_load(fd)
        
        
    srv = Server(cfg)

    signal.signal(signal.SIGINT, lambda *args: srv.shutdown())
    signal.signal(signal.SIGTERM, lambda *args: srv.shutdown())

    srv.run()
        

    
    
    
    
    
    



